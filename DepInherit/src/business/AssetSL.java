/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Adam Rich
 */
public class AssetSL extends Asset {
    private double anndep;
    private double[] begbal, endbal;
    private boolean built;
    
    public AssetSL() {
        super();
    }
    
    public AssetSL(String nm, double c, double s, int lf) {
        super(nm, c, s, lf);
        if(super.isValid()) {
            buildSL();
        }
    }
    
    public void buildSL() {
        try {
            this.anndep = (super.getCost() - super.getSalvage()) / super.getLife();
            
            this.begbal = new double[super.getLife()];
            this.endbal = new double[super.getLife()];

            this.begbal[0] = super.getCost();

            for(int i=0; i<super.getLife(); i++) {
                if(i>0) {
                    this.begbal[i] = this.endbal[i-1];
                }
                
                this.endbal[i] = this.begbal[i] - this.anndep;
            }
            this.built = true;
        } catch (Exception e) {
            super.setErrorMsg("Build Error: " + e.getMessage());
            this.built = false;
        }
    }
    
    public double getAnnDep() {
        if(!this.built) {
            buildSL();
            if(!this.built) {
                return -1;
            }
        }
        
        return this.anndep;
    }
    
    public double getBegBal(int yr) {
        if(!this.built) {
            buildSL();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }
        
        return this.begbal[yr-1];
    }
    
    public double getEndBal(int yr) {
        if(!this.built) {
            buildSL();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }
        
        return this.endbal[yr-1];
    }
}
