package business;

/**
 *
 * @author Adam Rich
 */
public class Asset {
    private String nm, emsg;
    private double cost, salvage;
    private int life;
    
    public Asset() {
        this.nm = "";
        this. cost = 0;
        this.salvage = 0;
        this.life = 0;
    }
    
    public Asset(String nm, double cost, double salvage, int life) {
        this.nm = nm;
        this. cost = cost;
        this.salvage = salvage;
        this.life = life;
        
        isValid();
    }
    
    protected boolean isValid() {
        this.emsg = "";
        
        if(this.nm.trim().isEmpty())
            this.emsg += "Asset name is missing. ";
        if(this.cost <= 0)
            this.emsg += "Cost must be > 0. ";
        if(this.salvage <= 0)
            this.emsg += "Salvage must be > 0. ";
        if(this.life <= 0)
            this.emsg += "Life must be > 0. ";
        if(this.salvage >= this.cost)
            this.emsg += "Salvage must be < cost.";
        
        return this.emsg.isEmpty();
    }

    
    public String getErrorMsg() {
        return this.emsg;
    }
    
    protected void setErrorMsg(String emsg) {
        this.emsg = emsg;
    }
        
    public int getLife() {
        return this.life;
    }
    
    public double getCost() {
        return this.cost;
    }
    
    public double getSalvage() {
        return this.salvage;
    }
    
    public String getName() {
        return this.nm;
    }
    
    public void setName(String name) {
        this.nm = name;
    }
    
    public void setCost(double cst) {
        this.cost = cst;
    }
    
    public void setSalvage(double sal) {
        this.salvage = sal;
    }
    
    public void setLife(int lf) {
        this.life = lf;
    }
}
