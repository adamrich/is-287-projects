/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Adam Rich
 */
public class Asset15DL extends AssetDL {
    private static final double FACTOR = 1.5;
    
    public Asset15DL() {
        super();
    }
    
    public Asset15DL(String nm, double c, double s, int lf) {
        super(nm, c, s, lf, FACTOR);
    }
}
