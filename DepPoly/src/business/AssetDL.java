/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author adam
 */
public class AssetDL extends Asset {
    private double[] anndep, begbal, endbal;
    private double factor;
    private boolean built;
    
    public AssetDL() {
        super();
    }
    
    public AssetDL(String nm, double c, double s, int lf, double factor) {
        super(nm, c, s, lf);
        
        this.factor = factor;
        
        if(super.isValid()) {
            buildDL();
        }
    }
    
    private void buildDL() {
        try {
            double anndepSL = (super.getCost() - super.getSalvage()) / super.getLife();
            double rateDDL = (1.0 / super.getLife()) * this.factor;
            double workDDL;
            
            this.begbal = new double[super.getLife()];
            this.anndep = new double[super.getLife()];
            this.endbal = new double[super.getLife()];

            this.begbal[0] = super.getCost();

            for(int i=0; i<super.getLife(); i++) {
                if(i>0) {
                    this.begbal[i] = this.endbal[i-1];
                }
                
                workDDL = this.begbal[i] * rateDDL;
                
                if(workDDL < anndepSL) {
                    workDDL = anndepSL;
                }
                
                if((this.begbal[i] - workDDL) < super.getSalvage()) {
                    workDDL = this.begbal[i] - super.getSalvage();
                }
                
                this.anndep[i] = workDDL;
                
                this.endbal[i] = this.begbal[i] - this.anndep[i];
            }
            this.built = true;
        } catch (Exception e) {
            super.setErrorMsg("Build Error: " + e.getMessage());
            this.built = false;
        }
    }
    
    @Override
    public double getAnnDep(int yr) {
        if(!this.built) {
            buildDL();
            if(!this.built) {
                return -1;
            }
        }
        
        return this.anndep[yr-1];
    }
    
    @Override
    public double getBegBal(int yr) {
        if(!this.built) {
            buildDL();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }
        
        return this.begbal[yr-1];
    }
    
    @Override
    public double getEndBal(int yr) {
        if(!this.built) {
            buildDL();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }

        return this.endbal[yr-1];
    }
}
