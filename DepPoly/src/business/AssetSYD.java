/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author adam
 */
public class AssetSYD extends Asset {
    private double[] anndep, begbal, endbal, anndeprate;
    private boolean built;
    
    public AssetSYD() {
        super();
    }
    
    public AssetSYD(String nm, double c, double s, int lf) {
        super(nm, c, s, lf);
                
        if(super.isValid()) {
            buildSYD();
        }
    }
    
    private void buildSYD() {
        try {
            double syd = super.getLife() * (super.getLife() + 1) / 2;
            double totdep = super.getCost() - super.getSalvage();
            
            this.begbal = new double[super.getLife()];
            this.anndep = new double[super.getLife()];
            this.endbal = new double[super.getLife()];
            this.anndeprate = new double[super.getLife()];

            this.begbal[0] = super.getCost();

            for(int i=0; i<super.getLife(); i++) {
                if(i>0) {
                    this.begbal[i] = this.endbal[i-1];
                }
                
                this.anndeprate[i] = (super.getLife() - i) / syd;
                
                this.anndep[i] = totdep * this.anndeprate[i];
                
                this.endbal[i] = this.begbal[i] - this.anndep[i];
            }
            this.built = true;
        } catch (Exception e) {
            super.setErrorMsg("Build Error: " + e.getMessage());
            this.built = false;
        }
    }
    
    @Override
    public double getAnnDep(int yr) {
        if(!this.built) {
            buildSYD();
            if(!this.built) {
                return -1;
            }
        }
        
        return this.anndep[yr-1];
    }
    
    @Override
    public double getBegBal(int yr) {
        if(!this.built) {
            buildSYD();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }
        
        return this.begbal[yr-1];
    }
    
    @Override
    public double getEndBal(int yr) {
        if(!this.built) {
            buildSYD();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }

        return this.endbal[yr-1];
    }
    
    public double getAnnDepRate(int yr) {
        if(!this.built) {
            buildSYD();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > super.getLife()) {
            return -1;
        }

        return this.anndeprate[yr-1];
    }
}
