/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Adam Rich
 */
public class AssetDDL extends AssetDL {    
    private static final double FACTOR = 2.0;
    
    public AssetDDL() {
        super();
    }
    
    public AssetDDL(String nm, double c, double s, int lf) {
        super(nm, c, s, lf, FACTOR);
    }
}
