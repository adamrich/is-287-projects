package business;

/**
 *
 * @author Adam Rich
 */
public class ConvCalc {
    public static final String MIDESC = "Miles";
    public static final String KMDESC = "Kilometers";
    public static final String OZDESC = "Ounces";
    public static final String GRDESC = "Grams";
    public static final String FDESC = "Fahrenheit";
    public static final String CDESC = "Celsius";
    public static final String KDESC = "Kelvin";
    public static final String FTDESC = "Feet";
    public static final String MDESC = "Meters";
    public static final String GALDESC = "Gallons";
    public static final String LDESC = "Liters";
    
    public static double getMiToKm(double miles) throws Exception {
        double km;
        
        if(miles >= 0)
            km = miles * 1.60934;
        else
            throw new Exception("Miles cannot be negative.");
        
        return km;
    }
    
    public static double getKmToMi(double km) throws Exception {
        double miles;
        
        if(km >= 0)
            miles = km / 1.60934;
        else
            throw new Exception("Kilometers cannot be negative.");
        
        return miles;
    }
    
    public static double getOzToG(double oz) throws Exception {
        double g;
        
        if(oz >= 0)
            g = oz * 28.3495;
        else
            throw new Exception("Ounces cannot be negative.");
        
        return g;
    }
    
    public static double getGToOz(double g) throws Exception {
        double oz;
        
        if(g >= 0)
            oz = g / 28.3495;
        else
            throw new Exception("Grams cannot be negative.");
        
        return oz;
    }
    
    public static double getFToC(double f) throws Exception {
        double c = (5.0/9.0)*(f-32);
        
        try {
            ConvCalc.getFToK(c);
        } catch(Exception e) {
            throw e;
        }
        
        return c;
    }
    
    public static double getCToF(double c) throws Exception {
        double f = ((9.0/5.0)*c)+32;
        
        try {
            ConvCalc.getCToK(c);
        } catch(Exception e) {
            throw e;
        }
        
        return f;
    }
    
    public static double getFToK(double f) throws Exception {
        double k = (5.0/9.0)*(f-32) + 273.15;
        
        if(k < 0)
            throw new Exception("Impossible temp: below absolute zero.");
        
        return k;
    }
    
    public static double getCToK(double c) throws Exception {
        double k = c + 273.15;
        
        if(k < 0)
            throw new Exception("Impossible temp: below absolute zero.");
        
        return k;
    }
    
    public static double getMtoFt(double m) throws Exception {
        double ft;
        
        if(m >= 0)
            ft = m * 3.2808;
        else
            throw new Exception("Meters cannot be negative.");
        
        return ft;
    }
    
    public static double getLtoGal(double l) throws Exception {
        double gal;
        
        if(l >= 0)
            gal = l * 0.26417;
        else
            throw new Exception("Liters cannot be negative.");
            
        return gal;
    }
}
