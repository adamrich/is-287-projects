/*
 * ConversionsOOPView.java
 */

package conversionsoop;

import business.ConvCalc;
import java.text.NumberFormat;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * The application's main frame.
 */
public class ConversionsOOPView extends FrameView {

    public ConversionsOOPView(SingleFrameApplication app) {
        super(app);

        initComponents();

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = ConversionsOOPApp.getApplication().getMainFrame();
            aboutBox = new ConversionsOOPAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        ConversionsOOPApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jradM2K = new javax.swing.JRadioButton();
        jradOz2G = new javax.swing.JRadioButton();
        jradF2C = new javax.swing.JRadioButton();
        jradK2M = new javax.swing.JRadioButton();
        jradG2Oz = new javax.swing.JRadioButton();
        jradC2F = new javax.swing.JRadioButton();
        jchkShowK = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        jtxtValue = new javax.swing.JTextField();
        jlblUnitsIn = new javax.swing.JLabel();
        jbtnConvert = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jtxtResult = new javax.swing.JTextField();
        jlblUnitsOut = new javax.swing.JLabel();
        jbtnClear = new javax.swing.JButton();
        jradM2Ft = new javax.swing.JRadioButton();
        jradL2Gal = new javax.swing.JRadioButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        buttonGroup1 = new javax.swing.ButtonGroup();

        mainPanel.setName("mainPanel"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(conversionsoop.ConversionsOOPApp.class).getContext().getResourceMap(ConversionsOOPView.class);
        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        buttonGroup1.add(jradM2K);
        jradM2K.setText(resourceMap.getString("jradM2K.text")); // NOI18N
        jradM2K.setToolTipText(resourceMap.getString("jradM2K.toolTipText")); // NOI18N
        jradM2K.setName("jradM2K"); // NOI18N
        jradM2K.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradM2KItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradOz2G);
        jradOz2G.setText(resourceMap.getString("jradOz2G.text")); // NOI18N
        jradOz2G.setName("jradOz2G"); // NOI18N
        jradOz2G.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradOz2GItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradF2C);
        jradF2C.setText(resourceMap.getString("jradF2C.text")); // NOI18N
        jradF2C.setName("jradF2C"); // NOI18N
        jradF2C.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradF2CItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradK2M);
        jradK2M.setText(resourceMap.getString("jradK2M.text")); // NOI18N
        jradK2M.setName("jradK2M"); // NOI18N
        jradK2M.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradK2MItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradG2Oz);
        jradG2Oz.setText(resourceMap.getString("jradG2Oz.text")); // NOI18N
        jradG2Oz.setName("jradG2Oz"); // NOI18N
        jradG2Oz.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradG2OzItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradC2F);
        jradC2F.setText(resourceMap.getString("jradC2F.text")); // NOI18N
        jradC2F.setName("jradC2F"); // NOI18N
        jradC2F.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradC2FItemStateChanged(evt);
            }
        });

        jchkShowK.setText(resourceMap.getString("jchkShowK.text")); // NOI18N
        jchkShowK.setName("jchkShowK"); // NOI18N

        jLabel2.setFont(resourceMap.getFont("jLabel2.font")); // NOI18N
        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setToolTipText(resourceMap.getString("jLabel2.toolTipText")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        jtxtValue.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtxtValue.setText(resourceMap.getString("jtxtValue.text")); // NOI18N
        jtxtValue.setName("jtxtValue"); // NOI18N

        jlblUnitsIn.setText(resourceMap.getString("jlblUnitsIn.text")); // NOI18N
        jlblUnitsIn.setName("jlblUnitsIn"); // NOI18N

        jbtnConvert.setText(resourceMap.getString("jbtnConvert.text")); // NOI18N
        jbtnConvert.setName("jbtnConvert"); // NOI18N
        jbtnConvert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnConvertActionPerformed(evt);
            }
        });

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setToolTipText(resourceMap.getString("jLabel4.toolTipText")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        jtxtResult.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtxtResult.setName("jtxtResult"); // NOI18N

        jlblUnitsOut.setText(resourceMap.getString("jlblUnitsOut.text")); // NOI18N
        jlblUnitsOut.setName("jlblUnitsOut"); // NOI18N

        jbtnClear.setText(resourceMap.getString("jbtnClear.text")); // NOI18N
        jbtnClear.setName("jbtnClear"); // NOI18N
        jbtnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnClearActionPerformed(evt);
            }
        });

        buttonGroup1.add(jradM2Ft);
        jradM2Ft.setText(resourceMap.getString("jradM2Ft.text")); // NOI18N
        jradM2Ft.setToolTipText(resourceMap.getString("jradM2Ft.toolTipText")); // NOI18N
        jradM2Ft.setName("jradM2Ft"); // NOI18N
        jradM2Ft.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradM2FtItemStateChanged(evt);
            }
        });

        buttonGroup1.add(jradL2Gal);
        jradL2Gal.setText(resourceMap.getString("jradL2Gal.text")); // NOI18N
        jradL2Gal.setToolTipText(resourceMap.getString("jradL2Gal.toolTipText")); // NOI18N
        jradL2Gal.setName("jradL2Gal"); // NOI18N
        jradL2Gal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jradL2GalItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jradM2K)
                        .addGap(18, 18, 18)
                        .addComponent(jradOz2G)
                        .addGap(18, 18, 18)
                        .addComponent(jradF2C)
                        .addGap(18, 18, 18)
                        .addComponent(jradM2Ft))
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(mainPanelLayout.createSequentialGroup()
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addComponent(jtxtValue, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jlblUnitsIn))
                                .addComponent(jbtnConvert, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(97, 97, 97)
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addComponent(jtxtResult, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jlblUnitsOut))
                                .addComponent(jbtnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)))
                        .addGroup(mainPanelLayout.createSequentialGroup()
                            .addComponent(jradK2M)
                            .addGap(18, 18, 18)
                            .addComponent(jradG2Oz)
                            .addGap(18, 18, 18)
                            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jchkShowK)
                                .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addComponent(jradC2F)
                                    .addGap(18, 18, 18)
                                    .addComponent(jradL2Gal))))))
                .addContainerGap(103, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jradM2K)
                    .addComponent(jradOz2G)
                    .addComponent(jradF2C)
                    .addComponent(jradM2Ft))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jradK2M, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jradG2Oz)
                        .addComponent(jradC2F))
                    .addComponent(jradL2Gal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jchkShowK)
                .addGap(80, 80, 80)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtxtValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlblUnitsIn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbtnConvert, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtxtResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlblUnitsOut))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbtnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(67, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(conversionsoop.ConversionsOOPApp.class).getContext().getActionMap(ConversionsOOPView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 223, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void jradM2KItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradM2KItemStateChanged
        if(jradM2K.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.MIDESC);
            jlblUnitsOut.setText(ConvCalc.KMDESC);
        }
    }//GEN-LAST:event_jradM2KItemStateChanged

    private void jradK2MItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradK2MItemStateChanged
        if(jradK2M.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.KMDESC);
            jlblUnitsOut.setText(ConvCalc.MIDESC);
        }
    }//GEN-LAST:event_jradK2MItemStateChanged

    private void jradOz2GItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradOz2GItemStateChanged
        if(jradOz2G.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.OZDESC);
            jlblUnitsOut.setText(ConvCalc.GRDESC);
        }
    }//GEN-LAST:event_jradOz2GItemStateChanged

    private void jradG2OzItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradG2OzItemStateChanged
        if(jradG2Oz.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.GRDESC);
            jlblUnitsOut.setText(ConvCalc.OZDESC);
        }
    }//GEN-LAST:event_jradG2OzItemStateChanged

    private void jradF2CItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradF2CItemStateChanged
        if(jradF2C.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.FDESC);
            jlblUnitsOut.setText(ConvCalc.CDESC);
        }
    }//GEN-LAST:event_jradF2CItemStateChanged

    private void jradC2FItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradC2FItemStateChanged
        if(jradC2F.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.CDESC);
            jlblUnitsOut.setText(ConvCalc.FDESC);
        }
    }//GEN-LAST:event_jradC2FItemStateChanged

    private void jbtnConvertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnConvertActionPerformed
        double v, r;
        
        NumberFormat fixed = NumberFormat.getNumberInstance();
        fixed.setMaximumFractionDigits(3);
        
        statusMessageLabel.setText("");
        
        try {
            v = Double.parseDouble(jtxtValue.getText());
        } catch(NumberFormatException e) {
            statusMessageLabel.setText("Input value is not numeric.");
            return;
        }
        
        try {
            if(jradM2K.isSelected()) {
                r = ConvCalc.getMiToKm(v);
                jtxtResult.setText(fixed.format(r));
            } else if(jradK2M.isSelected()) {
                r = ConvCalc.getKmToMi(v);
                jtxtResult.setText(fixed.format(r));
            } else if(jradOz2G.isSelected()) {
                r = ConvCalc.getOzToG(v);
                jtxtResult.setText(fixed.format(r));
            } else if(jradG2Oz.isSelected()) {
                r = ConvCalc.getGToOz(v);
                jtxtResult.setText(fixed.format(r));
            } else if(jradF2C.isSelected()) {
                r = ConvCalc.getFToC(v);
                jtxtResult.setText(fixed.format(r));
                
                if(jchkShowK.isSelected()) {
                    r = ConvCalc.getFToK(v);
                    statusMessageLabel.setText("which is also " + fixed.format(r) + " kelvin.");
                }
            } else if(jradC2F.isSelected()) {
                r = ConvCalc.getCToF(v);
                jtxtResult.setText(fixed.format(r));
                
                if(jchkShowK.isSelected()) {
                    r = ConvCalc.getCToK(v);
                    statusMessageLabel.setText("which is also " + fixed.format(r) + " kelvin.");
                }
            } else if(jradM2Ft.isSelected()) {
                r = ConvCalc.getMtoFt(v);
                jtxtResult.setText(fixed.format(r));
            } else if(jradL2Gal.isSelected()) {
                r = ConvCalc.getLtoGal(v);
                jtxtResult.setText(fixed.format(r));
            }
        } catch(Exception e) {
            statusMessageLabel.setText("Conversion Error: " + e.getMessage());
            jtxtResult.setText("****");
        }
    }//GEN-LAST:event_jbtnConvertActionPerformed

    private void jbtnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnClearActionPerformed
        jtxtValue.setText("");
        jtxtResult.setText("");
        jlblUnitsIn.setText("units");
        jlblUnitsOut.setText("units");
        buttonGroup1.clearSelection();
        jchkShowK.setSelected(false);
        statusMessageLabel.setText("");
    }//GEN-LAST:event_jbtnClearActionPerformed

    private void jradM2FtItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradM2FtItemStateChanged
        if(jradM2Ft.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.MDESC);
            jlblUnitsOut.setText(ConvCalc.FTDESC);
        }
    }//GEN-LAST:event_jradM2FtItemStateChanged

    private void jradL2GalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jradL2GalItemStateChanged
        if(jradL2Gal.isSelected()) {
            jlblUnitsIn.setText(ConvCalc.LDESC);
            jlblUnitsOut.setText(ConvCalc.GALDESC);
        }
    }//GEN-LAST:event_jradL2GalItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton jbtnClear;
    private javax.swing.JButton jbtnConvert;
    private javax.swing.JCheckBox jchkShowK;
    private javax.swing.JLabel jlblUnitsIn;
    private javax.swing.JLabel jlblUnitsOut;
    private javax.swing.JRadioButton jradC2F;
    private javax.swing.JRadioButton jradF2C;
    private javax.swing.JRadioButton jradG2Oz;
    private javax.swing.JRadioButton jradK2M;
    private javax.swing.JRadioButton jradL2Gal;
    private javax.swing.JRadioButton jradM2Ft;
    private javax.swing.JRadioButton jradM2K;
    private javax.swing.JRadioButton jradOz2G;
    private javax.swing.JTextField jtxtResult;
    private javax.swing.JTextField jtxtValue;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;
}
