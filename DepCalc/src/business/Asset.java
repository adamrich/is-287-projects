package business;

/**
 *
 * @author Adam Rich
 */
public class Asset {
    private String nm, emsg;
    private double cost, salvage;
    private int life;
    private boolean built;
    private double[][] begbal, anndep, endbal;
    
    
    private static final int SL=0;
    private static final int DDL=1;
    
    public Asset() {
        this.nm = "";
        this. cost = 0;
        this.salvage = 0;
        this.life = 0;
        this.built = false;
    }
    
    public Asset(String nm, double cost, double salvage, int life) {
        this.nm = nm;
        this. cost = cost;
        this.salvage = salvage;
        this.life = life;
        
        if(isValid())
            buildDep();
    }
    
    private boolean isValid() {
        this.emsg = "";
        
        if(this.nm.trim().isEmpty())
            this.emsg += "Asset name is missing. ";
        if(this.cost <= 0)
            this.emsg += "Cost must be > 0. ";
        if(this.salvage <= 0)
            this.emsg += "Salvage must be > 0. ";
        if(this.life <= 0)
            this.emsg += "Life must be > 0. ";
        if(this.salvage >= this.cost)
            this.emsg += "Salvage must be < cost.";
        
        return this.emsg.isEmpty();
    }
    
    private void buildDep() {
        try {
            double anndepSL = (this.cost - this.salvage) / this.life;
            double rateDDL = (1.0 / this.life) * 2;
            double workDDL = 0;
            
            this.begbal = new double[this.life][2];
            this.anndep = new double[this.life][2];
            this.endbal = new double[this.life][2];

            this.begbal[0][SL] = this.cost;
            this.begbal[0][DDL] = this.cost;

            for(int i=0; i<this.life; i++) {
                if(i>0) {
                    this.begbal[i][SL] = this.endbal[i-1][SL];
                    this.begbal[i][DDL] = this.endbal[i-1][DDL];
                }
                this.anndep[i][SL] = anndepSL;
                this.endbal[i][SL] = this.begbal[i][SL] - anndepSL;
                
                workDDL = this.begbal[i][DDL] * rateDDL;
                
                if(this.anndep[i][SL] <= workDDL) {
                    this.anndep[i][DDL] = workDDL;
                } else {
                    this.anndep[i][DDL] = this.anndep[i][SL];
                }
                
                workDDL = this.begbal[i][DDL] - this.anndep[i][DDL];
                if(workDDL < this.salvage) {
                    this.anndep[i][DDL] = this.begbal[i][DDL] - this.salvage;
                }
                
                this.endbal[i][DDL] = this.begbal[i][DDL] - this.anndep[i][DDL];
            }
            this.built = true;
        } catch (Exception e) {
            this.emsg = "Build Error: " + e.getMessage();
            this.built = false;
        }
    }
    
    public String getErrorMsg() {
        return this.emsg;
    }
        
    public int getLife() {
        return this.life;
    }
    
    public double getCost() {
        return this.cost;
    }
    
    public double getSalvage() {
        return this.salvage;
    }
    
    public String getName() {
        return this.nm;
    }
    
    public double getAnnDep() {
        if(!this.built) {
            buildDep();
            if(!this.built) {
                return -1;
            }
        }
        
        return this.anndep[0][SL];
    }
    
    public double getAnnDep(int yr) {
        if(!this.built) {
            buildDep();
            if(!this.built) {
                return -1;
            }
        }
        
        return this.anndep[yr-1][DDL];
    }
    
    public double getBegBal(int yr, String m) {
        if(!this.built) {
            buildDep();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > this.life) {
            return -1;
        }
        
        if(m.equalsIgnoreCase("S"))
            return this.begbal[yr-1][SL];
        else if(m.equalsIgnoreCase("D"))
            return this.begbal[yr-1][DDL];
        else
            return -1;
    }
    
    public double getEndBal(int yr, String m) {
        if(!this.built) {
            buildDep();
            if(!this.built) {
                return -1;
            }
        }
        
        if(yr < 1 || yr > this.life) {
            return -1;
        }
        
        if(m.equalsIgnoreCase("S"))
            return this.endbal[yr-1][SL];
        else if(m.equalsIgnoreCase("D"))
            return this.endbal[yr-1][DDL];
        else
            return -1;
    }
    
    public void setName(String name) {
        this.nm = name;
    }
    
    public void setCost(double cst) {
        this.cost = cst;
        buildDep();
    }
    
    public void setSalvage(double sal) {
        this.salvage = sal;
        buildDep();
    }
    
    public void setLife(int lf) {
        this.life = lf;
        buildDep();
    }
    
    public void setSave(String filename) {
        
    }
}
